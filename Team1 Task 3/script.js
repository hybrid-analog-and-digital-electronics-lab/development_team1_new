let xValue = [];
let yValue = [];
let operationValue = [];

let size = 0;

//Input of Slider 1
function xSlider()
{
    let xval = document.getElementById("inputvalx").value;
    document.getElementById("outputvalx").innerHTML = xval;
}

//Input for Slider 2
function ySlider()
{
    let yval = document.getElementById("inputvaly").value;
    document.getElementById("slidervaly").innerHTML = yval;
}

function addValue(){
	let xInput = parseFloat(document.getElementById("inputvalx").value);
    let yInput = parseFloat(document.getElementById("inputvaly").value);

    if(xInput == null || yInput == null)
        alert("please enter value in both the fields.");
    else
    {
        let operation = Math.log(xInput);

        xValue.push(xInput);
        yValue.push(yInput);
        operationValue.push(operation);

        //Now call function to add rows
        addRows("thisistable",xInput,yInput,operation);
    }
}

function addRows(tableID,xInput,yInput,operation1) {

    let tableRef = document.getElementById(tableID);
    // Insert a row at the end of the table
    let newRow = tableRef.insertRow(-1);

    // Insert a cell in the row at index 0,1,2
    let newCell0 = newRow.insertCell(0);
    let newCell1 = newRow.insertCell(1);
    let newCell2 = newRow.insertCell(2);

    // Append a text node to the cell 0
    let newText0 = document.createTextNode(String(xInput));
    newCell0.appendChild(newText0);

    // Append a text node to the cell 1
    let newText1 = document.createTextNode(String(yInput));
    newCell1.appendChild(newText1);

    // Append a text node to the cell 2
    let newText2 = document.createTextNode(operation1);
    newCell2.appendChild(newText2);

}

function makePlot(){

    if(xValue.length == 0 || yValue.length == 0){
        alert("enter some value in x and y");
    }

    else if(size < xValue.length){

        let c = [];

        for(let i = 0; i < xValue.length; i++) 
        {
            c.push(i)
        }
        
        let layout = {
            title: {
                text:'Graph of log(x) vs y',
                font: {
                  family: 'Courier New, monospace',
                  size: 24
                },
                xref: 'paper',
                x: 0.05,
              },
              xaxis: {
                title: {
                  text: 'y',
                  font: {
                    family: 'Courier New, monospace',
                    size: 18,
                  }
                },
              },
              yaxis: {
                title: {
                  text: 'log(x)',
                  font: {
                    family: 'Courier New, monospace',
                    size: 18,
                  }
                }
              },
            showlegend: false,
        };

        var trace1 = {
            x: yValue,
            y: operationValue,
            type: 'scatter'
          };
          
          var data = [trace1];
          
          Plotly.newPlot('myDiv', data, layout, {displaylogo: false});
        size = xValue.length;

    }
}